## Justificaciones

**Dar alguna solución sobre qué pasa con una llamada cuando no hay ningún empleado libre.**
 - Si el thread pool está definido para correr de forma concurrente más hilos que la cantidad total empleados, los hilos extras serán bloqueados por la BlockingQueue y esperaran a que se libere un empleado para ejecutar. Ver test DispatcherSmallEmployeeListTest
 - Si el thread pool está definido para correr de forma concurrente menos o igual cantidad de hilos que la cantidad total de empleados, los hilos extras serán administrados por el thread pool y recién se ejecutara un nuevo hilo cuando otro haya terminado y por lo tanto haya un empleado libre. Ver test DispatcherSmallPoolTest


**Dar alguna solución sobre qué pasa con una llamada cuando entran más de 10 llamadas concurrentes.**

* Si entran más de 10 llamadas concurrentes, los hilos extras creados serán manejados por el thread pool y dejados en cola para ir ejecutándolos a medida que otros vayan finalizando su ejecución.
* Si el thread pool de ejecución es mayor que el tamaño de la cola de empleado, serán bloqueados por la BlockingQueue hasta que se libere un empleado.
