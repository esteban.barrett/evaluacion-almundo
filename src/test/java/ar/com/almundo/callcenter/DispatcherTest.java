package ar.com.almundo.callcenter;

import ar.com.almundo.callcenter.Utils.Utils;
import ar.com.almundo.callcenter.model.Employee;
import ar.com.almundo.callcenter.services.CallWorkerRunnable;
import ar.com.almundo.callcenter.services.Dispatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class DispatcherTest {

    @Value("${numberOperators}")
    private int NUMBER_OPERATORS;

    @Value("${numberSupervisors}")
    private int NUMBER_SUPERVISORS;

    @Value("${numberDirectors}")
    private int NUMBER_DIRECTORS;

    @Autowired
    private Dispatcher dispatcher;

    @Autowired
    @Qualifier("taskExecutor")
    private ThreadPoolTaskExecutor taskExecutor;

    @Autowired
    private Queue<Employee> employeeQueue;

    @Test
    public void testExecuteTenCallsConcurrent() {
        int totalEmployees = NUMBER_OPERATORS + NUMBER_SUPERVISORS + NUMBER_DIRECTORS;
        int callsToAttended = 10;

        for (int i = 0; i < callsToAttended; i++) {
            dispatcher.receiveCall();
        }

        taskExecutor.shutdown();

        assertEquals(totalEmployees, employeeQueue.size());
        assertEquals(callsToAttended, dispatcher.getCallAttended().get());
    }

    @Test
    public void testExecuteSevenCallsConcurrent() {
        int totalEmployees = NUMBER_OPERATORS + NUMBER_SUPERVISORS + NUMBER_DIRECTORS;
        int callsToAttended = 7;

        for (int i = 0; i < callsToAttended; i++) {
            dispatcher.receiveCall();
        }
        taskExecutor.shutdown();

        assertEquals(totalEmployees, employeeQueue.size());
        assertEquals(callsToAttended, dispatcher.getCallAttended().get());
    }

    @Test
    public void testExecuteFifteenCallsConcurrent() {
        int totalEmployees = NUMBER_OPERATORS + NUMBER_SUPERVISORS + NUMBER_DIRECTORS;
        int callsToAttended = 15;

        for (int i = 0; i < callsToAttended; i++) {
            dispatcher.receiveCall();
        }
        taskExecutor.shutdown();

        assertEquals(totalEmployees, employeeQueue.size());
        assertEquals(callsToAttended, dispatcher.getCallAttended().get());
    }

    @Test
    public void testUtillRandomBetweenValues(){
        Long randomLongBetweenValues = Utils.getRandomLongBetweenValues(CallWorkerRunnable.MIN_DURATION_CALL, CallWorkerRunnable.MAX_DURATION_CALL);
        assertTrue("Esta en Rango!", randomLongBetweenValues >= CallWorkerRunnable.MIN_DURATION_CALL && randomLongBetweenValues <= CallWorkerRunnable.MAX_DURATION_CALL);

    }
}
