package ar.com.almundo.callcenter;

import ar.com.almundo.callcenter.model.Call;
import ar.com.almundo.callcenter.model.Employee;
import ar.com.almundo.callcenter.services.Dispatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Queue;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(properties = {
        "numberOperators=7",
        "numberSupervisors=2",
        "numberDirectors=1"
})
public class DispatcherSecuenciallyTest {

    @Autowired
    private Dispatcher dispatcher;

    @Autowired
    @Qualifier("taskExecutor")
    private ThreadPoolTaskExecutor taskExecutor;

    @Autowired
    private Queue<Employee> employeeQueue;

    @Test
    public void testDispatchAndEndCallsSecuentially() throws InterruptedException {
        Call call1 = new Call(1);
        Call call2 = new Call(2);
        Call call3 = new Call(3);
        Call call4 = new Call(4);
        Call call5 = new Call(5);
        Call call6 = new Call(6);
        Call call7 = new Call(7);
        Call call8 = new Call(8);
        Call call9 = new Call(9);
        Call call10 = new Call(10);

        dispatcher.dispatchCall(call1);
        assertEquals(9, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call1.getAssignedEmployee().getPriority()));

        dispatcher.dispatchCall(call2);
        assertEquals(8, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call2.getAssignedEmployee().getPriority()));

        dispatcher.dispatchCall(call3);
        assertEquals(7, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call3.getAssignedEmployee().getPriority()));

        dispatcher.dispatchCall(call4);
        assertEquals(6, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call4.getAssignedEmployee().getPriority()));

        dispatcher.dispatchCall(call5);
        assertEquals(5, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call5.getAssignedEmployee().getPriority()));

        dispatcher.dispatchCall(call6);
        assertEquals(4, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call6.getAssignedEmployee().getPriority()));

        dispatcher.dispatchCall(call7);
        assertEquals(3, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call7.getAssignedEmployee().getPriority()));
        // 7 operadores atendiendo llamadas

        dispatcher.dispatchCall(call8);
        assertEquals(2, employeeQueue.size());
        assertEquals(Employee.SUPERVISOR_PRIORITY, Integer.valueOf(call8.getAssignedEmployee().getPriority()));

        dispatcher.dispatchCall(call9);
        assertEquals(1, employeeQueue.size());
        assertEquals(Employee.SUPERVISOR_PRIORITY, Integer.valueOf(call9.getAssignedEmployee().getPriority()));
        // 2 supervisores atendiendo llamadas

        dispatcher.dispatchCall(call10);
        assertEquals(0, employeeQueue.size());
        assertEquals(Employee.DIRECTOR_PRIORITY, Integer.valueOf(call10.getAssignedEmployee().getPriority()));
        // 1 director atendiendo llamadas

        dispatcher.endCall(call1);
        dispatcher.endCall(call2);
        dispatcher.endCall(call3);
        dispatcher.endCall(call4);
        dispatcher.endCall(call5);
        dispatcher.endCall(call6);
        dispatcher.endCall(call7);
        dispatcher.endCall(call8);
        dispatcher.endCall(call9);
        dispatcher.endCall(call10);

        assertEquals(10, employeeQueue.size());
        assertEquals(10, dispatcher.getCallAttended().get());
    }

    @Test
    public void testPriorityQueueSecuentially() throws InterruptedException {
        Call call1 = new Call(1);
        Call call2 = new Call(2);
        Call call3 = new Call(3);
        Call call4 = new Call(4);
        Call call5 = new Call(5);
        Call call6 = new Call(6);
        Call call7 = new Call(7);

        dispatcher.dispatchCall(call1);
        assertEquals(9, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call1.getAssignedEmployee().getPriority()));

        dispatcher.dispatchCall(call2);
        assertEquals(8, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call2.getAssignedEmployee().getPriority()));

        dispatcher.dispatchCall(call3);
        assertEquals(7, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call3.getAssignedEmployee().getPriority()));

        dispatcher.dispatchCall(call4);
        assertEquals(6, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call4.getAssignedEmployee().getPriority()));

        dispatcher.dispatchCall(call5);
        assertEquals(5, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call5.getAssignedEmployee().getPriority()));

        dispatcher.dispatchCall(call6);
        assertEquals(4, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call6.getAssignedEmployee().getPriority()));

        dispatcher.dispatchCall(call7);
        assertEquals(3, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call7.getAssignedEmployee().getPriority()));

        //Se liberan 2 operadores
        dispatcher.endCall(call1);
        dispatcher.endCall(call2);

        Call call8 = new Call(8);
        Call call9 = new Call(9);
        Call call10 = new Call(10);

        // Es atendido por uno de los operadores que se libero
        dispatcher.dispatchCall(call8);
        assertEquals(4, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call8.getAssignedEmployee().getPriority()));

        // Es atendido por el otro de los operadores que se libero
        dispatcher.dispatchCall(call9);
        assertEquals(3, employeeQueue.size());
        assertEquals(Employee.OPERATOR_PRIORITY, Integer.valueOf(call9.getAssignedEmployee().getPriority()));

        // No quedan operadores libres lo debe atender un supervisor
        dispatcher.dispatchCall(call10);
        assertEquals(2, employeeQueue.size());
        assertEquals(Employee.SUPERVISOR_PRIORITY, Integer.valueOf(call10.getAssignedEmployee().getPriority()));

    }

}
