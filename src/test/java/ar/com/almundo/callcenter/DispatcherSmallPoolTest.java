package ar.com.almundo.callcenter;

import ar.com.almundo.callcenter.model.Employee;
import ar.com.almundo.callcenter.services.Dispatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Queue;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(properties = {
        "numberOperators=7",
        "numberSupervisors=2",
        "numberDirectors=1",
        "threadPoolSize=5",
        "useNumberEmployeesAsThreadPoolSize=false"
})
public class DispatcherSmallPoolTest {

    @Value("${numberOperators}")
    private int NUMBER_OPERATORS;

    @Value("${numberSupervisors}")
    private int NUMBER_SUPERVISORS;

    @Value("${numberDirectors}")
    private int NUMBER_DIRECTORS;

    @Autowired
    private Dispatcher dispatcher;

    @Autowired
    @Qualifier("taskExecutor")
    private ThreadPoolTaskExecutor taskExecutor;

    @Autowired
    private Queue<Employee> employeeQueue;

    @Test
    public void testExecuteTenCallsConcurrentWithSmallExecutorPool() {
        int totalEmployees = NUMBER_OPERATORS + NUMBER_SUPERVISORS + NUMBER_DIRECTORS;
        int callsToAttended = 10;

        for (int i = 0; i < callsToAttended; i++) {
            dispatcher.receiveCall();
        }

        taskExecutor.shutdown();

        assertEquals(totalEmployees, employeeQueue.size());
        assertEquals(callsToAttended, dispatcher.getCallAttended().get());
    }


}
