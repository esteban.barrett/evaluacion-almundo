package ar.com.almundo.callcenter.services;

import ar.com.almundo.callcenter.Utils.Utils;
import ar.com.almundo.callcenter.model.Call;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class CallWorkerRunnable implements Runnable {

    private Call callToAttend;

    private Dispatcher dispatcher;

    public static final Long MAX_DURATION_CALL = 10000L;
    public static final Long MIN_DURATION_CALL = 5000L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CallWorkerRunnable.class);

    public CallWorkerRunnable(Call callToAttend, Dispatcher dispatcher) {
        this.callToAttend = callToAttend;
        this.dispatcher = dispatcher;
    }

    @Override
    public void run() {

        try {
            dispatcher.dispatchCall(callToAttend);

            LOGGER.info(String.format("Call - %d is answered by %s", callToAttend.getId(), callToAttend.getAssignedEmployee().getName()));

            Long randomTime = Utils.getRandomLongBetweenValues(MIN_DURATION_CALL, MAX_DURATION_CALL);
            callToAttend.setCallTime(randomTime);

            Thread.sleep(randomTime);

        } catch (InterruptedException e) {
            LOGGER.error(String.format("Call - %d was interrupted", callToAttend.getId()));
            e.printStackTrace();
        } finally {
            if (Optional.ofNullable(callToAttend.getAssignedEmployee()).isPresent())
                dispatcher.endCall(callToAttend);
            LOGGER.info(String.format("Call - %d is finished in %d ms", callToAttend.getId(), callToAttend.getCallTime()));
        }

    }

}
