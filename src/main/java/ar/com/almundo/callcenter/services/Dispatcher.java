package ar.com.almundo.callcenter.services;

import ar.com.almundo.callcenter.model.Call;
import ar.com.almundo.callcenter.model.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class Dispatcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(Dispatcher.class);

    @Autowired
    @Qualifier("taskExecutor")
    private ThreadPoolTaskExecutor taskExecutor;

    @Autowired
    @Qualifier("employeeQueue")
    private PriorityBlockingQueue<Employee> employeeQueue;

    private final AtomicLong counter = new AtomicLong();

    private AtomicLong callAttended = new AtomicLong();

    public void  dispatchCall(Call call) throws InterruptedException {
        call.setAssignedEmployee(employeeQueue.take());
    }

    public void receiveCall(){
        Call call = new Call(counter.getAndIncrement());

        CallWorkerRunnable callWorkerRunnable = new CallWorkerRunnable(call, this);
        taskExecutor.execute(callWorkerRunnable);
    }

    public void endCall(Call call){
        employeeQueue.put(call.getAssignedEmployee());
        callAttended.incrementAndGet();
    }

    public AtomicLong getCallAttended() {
        return callAttended;
    }
}
