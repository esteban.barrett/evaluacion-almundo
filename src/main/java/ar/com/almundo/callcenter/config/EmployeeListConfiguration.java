package ar.com.almundo.callcenter.config;

import ar.com.almundo.callcenter.model.Director;
import ar.com.almundo.callcenter.model.Employee;
import ar.com.almundo.callcenter.model.Operator;
import ar.com.almundo.callcenter.model.Supervisor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.PriorityBlockingQueue;
import java.util.stream.IntStream;

@Configuration
public class EmployeeListConfiguration {

    @Value("${numberOperators}")
    private int NUMBER_OPERATORS;

    @Value("${numberSupervisors}")
    private int NUMBER_SUPERVISORS;

    @Value("${numberDirectors}")
    private int NUMBER_DIRECTORS;

    @Bean(name = "employeeQueue")
    public PriorityBlockingQueue<Employee> employeeQueue(){

        PriorityBlockingQueue<Employee> employees = new PriorityBlockingQueue<>();

        IntStream.rangeClosed(1,NUMBER_OPERATORS).forEach(i -> employees.add(new Operator(String.format("Operador - %d", i))));
        IntStream.rangeClosed(1,NUMBER_SUPERVISORS).forEach(i -> employees.add(new Supervisor(String.format("Supervisor - %d", i))));
        IntStream.rangeClosed(1,NUMBER_DIRECTORS).forEach(i -> employees.add(new Director(String.format("Director - %d", i))));

        return employees;
    }
}
