package ar.com.almundo.callcenter.config;

import ar.com.almundo.callcenter.services.CallWorkerRunnable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Configuration
public class ExecutorConfig {

    @Value("${numberOperators}")
    private int NUMBER_OPERATORS;

    @Value("${numberSupervisors}")
    private int NUMBER_SUPERVISORS;

    @Value("${numberDirectors}")
    private int NUMBER_DIRECTORS;

    @Value("${threadPoolSize}")
    private int THREAD_POOL_SIZE;

    @Value("${useNumberEmployeesAsThreadPoolSize}")
    private boolean USE_EMPLOYEES_SIZE;

    @Bean(name = "taskExecutor")
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {

        int employeesNumber = USE_EMPLOYEES_SIZE ? NUMBER_OPERATORS + NUMBER_SUPERVISORS + NUMBER_DIRECTORS : THREAD_POOL_SIZE;

        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(employeesNumber);
        executor.setMaxPoolSize(employeesNumber * 2);
        executor.setThreadNamePrefix("call_executor_thread");
        executor.setWaitForTasksToCompleteOnShutdown(true);

        //Se define un tiempo estimado para que terminen de ejecutar
        executor.setAwaitTerminationSeconds((int) (employeesNumber * 2 * TimeUnit.MILLISECONDS.toSeconds(CallWorkerRunnable.MAX_DURATION_CALL)));
        executor.initialize();
        return executor;
    }
}
