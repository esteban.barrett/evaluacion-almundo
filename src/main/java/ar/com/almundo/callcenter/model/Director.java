package ar.com.almundo.callcenter.model;

public class Director extends Employee {

	public Director(String name) {
		super(name, Employee.DIRECTOR_PRIORITY);
	}
}
