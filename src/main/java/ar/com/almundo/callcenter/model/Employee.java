package ar.com.almundo.callcenter.model;

public abstract class Employee implements Comparable<Employee>{

	public static Integer OPERATOR_PRIORITY = 1;
	public static Integer SUPERVISOR_PRIORITY = 2;
	public static Integer DIRECTOR_PRIORITY = 3;

	private String name;
	private final Integer priority;

	Employee(String name, int priority) {
		this.name = name;
		this.priority = priority;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPriority() {
		return priority;
	}

	@Override public int compareTo(Employee o) {
		return this.priority.compareTo(o.priority);
	}
}
