package ar.com.almundo.callcenter.model;

public class Operator extends Employee {

	public Operator(String name) {
		super(name, Employee.OPERATOR_PRIORITY);
	}
}
