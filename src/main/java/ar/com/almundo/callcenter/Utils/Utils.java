package ar.com.almundo.callcenter.Utils;

import java.util.Random;

public class Utils {

    public static Long getRandomLongBetweenValues(Long minValue, Long maxValue){
        return  new Random().longs(minValue, (maxValue + 1)).limit(1).findFirst().getAsLong();
    }

}
