package ar.com.almundo.callcenter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CallcenterApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(CallcenterApplication.class);


    public static void main(String[] args) {
        LOGGER.info("Init Application");
        SpringApplication.run(CallcenterApplication.class, args);
    }

}
